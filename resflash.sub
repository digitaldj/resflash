#!/bin/sh

# Subroutines for all resflash tools
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

# Check whether -e is superfluous for handling escape characters
if [ $(echo "\n"|wc -l) -eq 2 ]; then
  ESCECHO=echo
else
  ESCECHO='echo -e'
fi

# Assigns: NEXTVND
get_next_vnd() {
  NEXTVND=$(vnconfig -l|grep 'not in use'|head -n 1|cut -d : -f 1)
}

umount_all() {
  sync

  for cleanupmnt in $(mount|tail -r|grep resflash|cut -d ' ' -f 3); do
    umount ${cleanupmnt}
  done

  for cleanupvnd in $(vnconfig -l|grep resflash|cut -d : -f 1); do
    vnconfig -u ${cleanupvnd}
  done
}

# Args: 1 - img or fs file
# Assigns: MNTPATH
mount_img_or_fs() {
  MNTPATH=$(mktemp -t -d resflash.XXXXXX)
  trap "umount_all; echo \*\*\* Error encountered. MNTPATH: ${MNTPATH} \*\*\*; \
  exit 1" ERR INT
  mkdir -p ${MNTPATH}/fs

  get_next_vnd
  mntvnd=${NEXTVND}
  vnconfig ${mntvnd} ${1} > /dev/null

  case $(echo ${1}|awk -F . -safe '{ print $NF }') in
    img) mkdir -p ${MNTPATH}/mbr ${MNTPATH}/cfg
         mount -o noatime /dev/${mntvnd}a ${MNTPATH}/mbr
         mount -o noatime /dev/${mntvnd}d ${MNTPATH}/fs
         mount -o noatime /dev/${mntvnd}f ${MNTPATH}/cfg
         if disklabel ${mntvnd}|grep -q 'i:.*MSDOS'; then
           mkdir -p ${MNTPATH}/msdos
           mount -o noatime /dev/${mntvnd}i ${MNTPATH}/msdos
         fi;;
    fs) mount -o noatime /dev/${mntvnd}c ${MNTPATH}/fs;;
    *) echo '*** ERROR: Not a resflash img or fs file. ***'
       vnconfig -u ${mntvnd}
       exit 1;;
  esac

  mount -t mfs -o noatime,nodev,noexec,-s256M swap ${MNTPATH}/fs/tmp
}

# Args: 1 - MACHINE
# Assigns: ALG, DOS{PARTID,MNT,BOOTDIR,BOOTBINS,PARTMB}, FLAGPART, MBRPARTMB,
# XXHALG
set_attr_by_machine() {
  case ${1} in
    amd64) ALG=sha512/256
           DOSPARTID=EF
           DOSSTART=64
           DOSPARTMB=3
           FLAGPART=3
           DOSMNT=efi
           DOSBOOTDIR=efi/boot
           DOSBOOTBINS='BOOTX64.EFI BOOTIA32.EFI'
           MBRSTART=4
           MBRPARTMB=4;;
    i386) ALG=md5
          DOSPARTID=EF
          DOSSTART=64
          DOSPARTMB=3
          FLAGPART=3
          DOSMNT=efi
          DOSBOOTDIR=
          DOSBOOTBINS=
          MBRSTART=4
          MBRPARTMB=4;;
    arm64) ALG=sha512/256
           DOSPARTID=0C
           # This offset is required by Rockchip
           DOSSTART=$((16 * 1024 * 1024 / BYTESECT))
           DOSPARTMB=16
           FLAGPART=0
           DOSMNT=uboot
           DOSBOOTDIR=efi/boot
           DOSBOOTBINS='BOOTAA64.EFI'
           MBRSTART=32
           MBRPARTMB=4;;
    *) echo 'Unsupported arch.'
       exit 1;;
  esac

  if which xxhsum > /dev/null 2>&1; then
    XXHALG=3
  fi
}
