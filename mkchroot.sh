#!/bin/sh

# Perform chroot activities on the mounted primary filesystem
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

# Set up a temporary /tmp

mount -t mfs -o noatime,nodev,noexec,-s256M swap ${BUILDPATH}/fs/tmp

# Run syspatch if directed

if [ -n "${syspatch+1}" ]; then
  echo -n "  Relinking kernel and running syspatch"
  st=$(date +%s)

  chroot ${BUILDPATH}/fs cksum -a sha256 -h /var/db/kernel.SHA256 /bsd
  if ! chroot ${BUILDPATH}/fs /usr/libexec/reorder_kernel; then
    ${ESCECHO} "\n*** WARNING: Relink failed, check" \
    "${BUILDPATH}/02.mkchroot.00.relink for details. ***"
  fi
  cp ${BUILDPATH}/fs/usr/share/relink/kernel/${KERNEL}/relink.log \
  ${BUILDPATH}/02.mkchroot.00.relink

  if [ $(chroot ${BUILDPATH}/fs syspatch -c|wc -l) -ne 0 ] && \
  ! chroot ${BUILDPATH}/fs syspatch >> ${BUILDPATH}/02.mkchroot.01.syspatch \
  2>&1; then
    ${ESCECHO} "\n*** WARNING: Syspatch failed, check" \
    "${BUILDPATH}/02.mkchroot.01.syspatch for connectivity and version" \
    "compatibility. ***"
  fi
  rm ${BUILDPATH}/fs/var/db/kernel.SHA256
  echo " ($(($(date +%s) - st))s)"
fi

# Preload ld.so.hints for potential package installation

if [ -n "${pkgdir+1}" ] || [ -n "${pkglist+1}" ]; then
  if [ -d ${BUILDPATH}/fs/usr/X11R6/lib ]; then
    chroot ${BUILDPATH}/fs ldconfig /usr/X11R6/lib /usr/local/lib
  else
    chroot ${BUILDPATH}/fs ldconfig /usr/local/lib
  fi
fi

# Install packages from pkgdir if directed, must run before pkglist to support
# compiled dependencies

if [ -n "${pkgdir+1}" ]; then
  echo -n "  Installing packages: ${pkgdir}"
  st=$(date +%s)
  mkdir -p ${BUILDPATH}/fs/tmp/pkg
  cp ${pkgdir}/*.tgz ${BUILDPATH}/fs/tmp/pkg/

  if ! chroot ${BUILDPATH}/fs sh -c "env PKG_PATH=${pkgpath} pkg_add -I -v -D \
  unsigned /tmp/pkg/*.tgz" >> ${BUILDPATH}/02.mkchroot.02.pkg_add.pkg_dir \
  2>&1; then
    ${ESCECHO} "\n*** WARNING: Package installation failed, check" \
    "${BUILDPATH}/02.mkchroot.02.pkg_add.pkg_dir for binary compatibility. ***"
  fi
  echo " ($(($(date +%s) - st))s)"
fi

# Install packages from pkgpath/pkglist if directed

if [ -n "${pkglist+1}" ]; then
  echo -n "  Installing packages: ${pkgpath}"
  st=$(date +%s)

  if ! chroot ${BUILDPATH}/fs sh -c "env PKG_PATH=${pkgpath} pkg_add -I -v \
  $(echo ${pkglist}|tr , ' ')" >> ${BUILDPATH}/02.mkchroot.03.pkg_add.pkg_list \
  2>&1; then
    ${ESCECHO} "\n*** WARNING: Package installation failed, check" \
    "${BUILDPATH}/02.mkchroot.03.pkg_add.pkg_list for connectivity and" \
    "version compatibility. ***"
  fi
  echo " ($(($(date +%s) - st))s)"
fi

# Clean up

umount ${BUILDPATH}/fs/tmp
